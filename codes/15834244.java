/**  
 * 冒泡排序函数  
 * 对数组a进行冒泡排序，将其变为有序数组  
 * @param a 待排序的数组  
 * @param n 待排序的数组长度  
 */  
public static void bubbleSort(int[] a, int n) {  
    for (int i = 0; i < n - 1; i++) { // 外层循环，控制排序趟数  
        for (int j = 0; j < n - i - 1; j++) { // 内层循环，进行相邻元素比较和交换  
            if (a[j] > a[j + 1]) { // 如果当前元素大于下一个元素，则交换它们  
                // 交换a[j]和a[j+1]  
                int temp = a[j];  
                a[j] = a[j + 1];  
                a[j + 1] = temp;  
            }  
        }  
    }  
} //end
