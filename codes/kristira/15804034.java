/**  
 * 冒泡排序函数  
 * 通过相邻元素两两比较和交换，使得每一轮过后最大（或最小）的元素被交换到序列末尾  
 * @param a 待排序的数组  
 * @param n 待排序的数组长度  
 */  
public static void bubbleSort(int [] a, int n){  
    for (int i = 0; i < n - 1; i++) { // 外层循环控制排序的轮数  
        for (int j = 0; j < n - 1 - i; j++) { // 内层循环控制每轮的比较次数  
            if (a[j] > a[j + 1]) { // 如果前一个元素大于后一个元素，则交换它们  
                // 交换 a[j] 和 a[j+1]  
                int temp = a[j];  
                a[j] = a[j + 1];  
                a[j + 1] = temp;  
            }  
        }  
    }  
} //end
