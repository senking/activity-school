/**
 * 冒泡排序函数
 * aa bb cc
 * @param a 待排序的数组
 * @param n 待排序的数组长度
 */
public static void bubbleSort(int [] a, int n) {
	// 你的代码，使无序数组 a 变得有序
	for (int i=1; i<=n; i++)
		for(int j=0; j<n-i; j++) 
			if(a[j]>a[j+1]) {
				int t=a[j];
				a[j]=a[j+1];
				a[j+1]=t;
			}

} //end
