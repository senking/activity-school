/**  
 * 冒泡排序函数  
 * 将无序数组通过冒泡排序变为有序数组  
 * @param a 待排序的数组  
 * @param n 待排序的数组长度  
 */  
public static void bubbleSort(int[] a, int n) {  
    for (int i = 0; i < n - 1; i++) { // 外层循环，控制比较轮数  
        for (int j = 0; j < n - 1 - i; j++) { // 内层循环，进行相邻元素比较  
            if (a[j] > a[j + 1]) { // 如果前一个元素大于后一个元素，则交换它们  
                // 交换 a[j] 和 a[j+1]  
                int temp = a[j];  
                a[j] = a[j + 1];  
                a[j + 1] = temp;  
            }  
        }  
    }  
} // end
