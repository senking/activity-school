/**  
 * 冒泡排序函数  
 * 通过相邻元素之间的比较和交换，使得每一轮比较后最大（或最小）的元素能够"冒泡"到数组的末尾。  
 * @param a 待排序的数组  
 * @param n 待排序的数组长度  
 */  
public static void bubbleSort(int[] a, int n) {  
    for (int i = 0; i < n - 1; i++) { // 外层循环控制排序趟数  
        for (int j = 0; j < n - i - 1; j++) { // 内层循环控制每一趟排序多少次  
            if (a[j] > a[j + 1]) { // 如果前一个元素比后一个元素大，则交换它们的位置  
                // 交换 a[j] 和 a[j+1]  
                int temp = a[j];  
                a[j] = a[j + 1];  
                a[j + 1] = temp;  
            }  
        }  
    }  
} //end
