/**  
 * 冒泡排序函数  
 * 通过重复地遍历待排序的数列，一次比较两个元素，如果他们的顺序错误就把他们交换过来。  
 * 遍历数列的工作是重复地进行直到没有再需要交换，也就是说该数列已经排序完成。  
 * 这个算法的名字由来是因为越小的元素会经由交换慢慢“浮”到数列的顶端。  
 * @param a 待排序的数组  
 * @param n 待排序的数组长度  
 */  
public static void bubbleSort(int[] a, int n) {  
    for (int i = 0; i < n - 1; i++) {  
        // 标志位，表示这一趟是否有交换，优化冒泡排序性能  
        boolean swapped = false;  
        for (int j = 0; j < n - i - 1; j++) {  
            // 如果前一个元素大于后一个元素，则交换它们  
            if (a[j] > a[j + 1]) {  
                // 交换 a[j] 和 a[j+1]  
                int temp = a[j];  
                a[j] = a[j + 1];  
                a[j + 1] = temp;  
                // 表示有数据交换，即这一趟排序有意义  
                swapped = true;  
            }  
        }  
        // 如果没有数据交换，说明已经有序，可以提前退出循环  
        if (!swapped) {  
            break;  
        }  
    }  
} //end
