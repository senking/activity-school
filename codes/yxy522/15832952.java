/**  
 * 冒泡排序函数  
 * 通过相邻元素比较和交换，将最大（或最小）的元素“冒泡”到数组的末尾。  
 * @param a 待排序的数组  
 * @param n 待排序的数组长度  
 */  
public static void bubbleSort(int[] a, int n) {  
    for (int i = 0; i < n - 1; i++) { // 外层循环控制需要排序的趟数  
        for (int j = 0; j < n - i - 1; j++) { // 内层循环控制每一趟排序的过程  
            if (a[j] > a[j + 1]) { // 相邻元素两两比较，如果前一个比后一个大，则交换它们  
                int temp = a[j];  
                a[j] = a[j + 1];  
                a[j + 1] = temp;  
            }  
        }  
    }  
} // end
