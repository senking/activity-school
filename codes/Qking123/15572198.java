/**
 * 冒泡排序函数
 * aa bb cc
 * @param a 待排序的数组
 * @param n 待排序的数组长度
 */
/*pulick static void bubbleSort(int []a, int n) {
        boolean flag = false;
        for (int i = 0; i < n; i++) {
            // 每轮遍历将最大的数移到末尾
            for (int j = 0; j < n - i - 1; j++) {
                if (a[j] > a[j+1]) {
                    int temp = a[j];
                    a[j] = a[j+1];
                    a[j+1] = temp;
                }
            }
            if (flag == false){
                break;
            }else {
                flag = false;
            }
  	}
} //end
*/
public static void bubbleSort(int [] a, int n){
    // 你的代码，使无序数组 a 变得有序
   for (int i = 0; i < n; i++) {
            for (int j = i + 1; j < n; j++) {
                if (a[i] > a[j]) {
                    int temp = a[j];
                    a[j] = a[i];
                    a[i] = temp;

                }
            }
        }

} //end
