/**  
 * 冒泡排序函数  
 * 对数组进行升序排序  
 * @param a 待排序的数组  
 * @param n 待排序的数组长度  
 */  
public static void bubbleSort(int[] a, int n) {  
    for (int i = 0; i < n - 1; i++) {  
        // 标志位，表示这一趟排序是否有数据交换  
        boolean swapped = false;  
        for (int j = 0; j < n - i - 1; j++) {  
            // 如果当前元素大于下一个元素，则交换它们  
            if (a[j] > a[j + 1]) {  
                // 交换 a[j] 和 a[j+1]  
                int temp = a[j];  
                a[j] = a[j + 1];  
                a[j + 1] = temp;  
                // 表示有数据交换，需要继续下一趟排序  
                swapped = true;  
            }  
        }  
        // 如果在内层循环中没有数据交换，则说明数组已经有序，可以提前退出  
        if (!swapped) {  
            break;  
        }  
    }  
} //end
