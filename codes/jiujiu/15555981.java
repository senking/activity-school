/**  
 * 冒泡排序函数  
 * 将无序数组通过冒泡排序算法变得有序  
 * @param a 待排序的数组  
 * @param n 待排序的数组长度  
 */  
public static void bubbleSort(int[] a, int n) {  
    // 外层循环控制排序趟数  
    for (int i = 0; i < n - 1; i++) {  
        // 内层循环负责每次趟的比较和交换  
        for (int j = 0; j < n - i - 1; j++) {  
            // 如果前一个元素比后一个元素大，则交换它们的位置  
            if (a[j] > a[j + 1]) {  
                int temp = a[j];  
                a[j] = a[j + 1];  
                a[j + 1] = temp;  
            }  
        }  
    }  
    // 数组a现在已经变得有序  
} //end
